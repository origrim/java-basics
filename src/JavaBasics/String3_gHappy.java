package JavaBasics;

public class String3_gHappy {
    public boolean gHappy(String str) {
        for (int i=0;i<str.length();i++)
        {
            if ((str.substring(i,i+1).equals("g"))&&
                    ((i==0)||(!str.substring(i-1,i).equals("g")))&&
                    ((i==str.length()-1)||(!str.substring(i+1,i+2).equals("g"))))
                return false;
        }
        return true;
    }
}

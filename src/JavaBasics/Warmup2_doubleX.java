package JavaBasics;

public class Warmup2_doubleX {
    boolean doubleX(String str) {

        if (str.length()<2){
            return false;
        }
        int index = str.indexOf("x");

        char c = ' ';
        if ((index >= 0) && (index < str.length()-1))
            c = str.charAt(index+1);

        if(c == 'x'){
            return true;
        }
        else {
            return false;
        }
    }
}

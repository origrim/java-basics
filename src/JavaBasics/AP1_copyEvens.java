package JavaBasics;

public class AP1_copyEvens {
    public int[] copyEvens(int[] nums, int count) {

        int[] newNums = new int[count];
        int j = 0;
        for (int i = 0; i < nums.length && j < count; i++){ //
            if (nums[i]%2==0)
                newNums[j++] = nums[i];
        }
        return newNums;
    }
}
